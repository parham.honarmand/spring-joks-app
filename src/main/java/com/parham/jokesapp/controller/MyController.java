package com.parham.jokesapp.controller;

import com.parham.jokesapp.service.Services;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MyController {
    private Services service;

    public MyController(Services service) {
        this.service = service;
    }

    @RequestMapping("")
    public String getJoke(Model model){
        model.addAttribute("joke", service.getJoke());
        return "view";
    }
}
