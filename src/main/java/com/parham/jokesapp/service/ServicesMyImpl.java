package com.parham.jokesapp.service;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Profile("local")
@Service("joke")
public class ServicesMyImpl implements Services {
    @Override
    public String getJoke() {
        return "that was hilarious";
    }
}
