package com.parham.jokesapp.service;

import guru.springframework.norris.chuck.ChuckNorrisQuotes;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Profile({"global", "default"})
@Service("joke")
public class ServiceAPIImpl implements Services {
    private final ChuckNorrisQuotes chuckNorrisQuotes;

    public ServiceAPIImpl() {
        this.chuckNorrisQuotes = new ChuckNorrisQuotes();
    }

    @Override
    public String getJoke() {
        return chuckNorrisQuotes.getRandomQuote();
    }
}
